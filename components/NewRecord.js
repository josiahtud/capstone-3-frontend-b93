import React from 'react';
import styles from '../styles/Home.module.css';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import Link from 'next/link';

export default function NewRecord(){

	// console.log(data);
	// const { title, content, destination, label } = data;

	return (
		<Form className={styles.container1}>
		<Link href="/records/addRecord">
			 <Button variant="success" size="lg" type="submit">New Record</Button>
		</Link>
		</Form>
		)
}
	