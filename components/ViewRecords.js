import React from 'react';
import styles from '../styles/Home.module.css';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import Link from 'next/link';

export default function ViewRecords(){

	// console.log(data);
	// const { title, content, destination, label } = data;

	return (
		<Form className={styles.container2}>
		<Link href="/records/view">
			 <Button variant="info" size="lg" type="submit">View Records</Button>
		</Link>
		</Form>
		)
}
	
