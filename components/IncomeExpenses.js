import React from 'react';
import styles from '../styles/Home.module.css';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';

export default function IncomeExpenses(){

  return (
  	<React.Fragment>
  	<Row className={styles.align}>
  		<Col  xs={6} md={4}>
  			<Card className={styles.cardincome}>
  			<Card.Body>
  				<Card.Title><h4>Income</h4></Card.Title>
  				<Card.Text id="money-plus">+P0.00</Card.Text>
  			</Card.Body>
  			</Card>
  		</Col>

  		<Col  xs={6} md={4}>
  			<Card className={styles.cardexpenses}>
  			<Card.Body>
  				<Card.Title><h4>Expenses</h4></Card.Title>
  				<Card.Text id="money-minus">-P0.00</Card.Text>
  			</Card.Body>
  			</Card>
  		</Col>
  	</Row>

    </React.Fragment>
  )
}

//className={styles.container}