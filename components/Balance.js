import React from 'react';
import styles from '../styles/Home.module.css';

export default function Balance(){

  return (
  	<React.Fragment>
  	<div className={styles.container}>
  		<h4 >Balance</h4>
  		<h1 id="balance">P0.00</h1>
  	</div>
    </React.Fragment>
  )
}
