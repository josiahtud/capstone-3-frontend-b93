import React, { useContext } from 'react';
import Link from 'next/link';
// import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NaviBar(){

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
		  <Link href="/">
		  <a className="navbar-brand">Budget Tracker</a>
		  </Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
				  <Link href="/">
				  <a className="nav-link" role="button">Home</a>
				  </Link>
				  <Link href="/records/view">
				  <a className="nav-link" role="button">Records</a>
				  </Link>
		      {(user.email !== null) ? 
		      	<React.Fragment>
		      		{user.isAdmin ? 
		      		<React.Fragment>
					  <Link href="/records/create">
					  <a className="nav-link" role="button">Create Record</a>
					  </Link>
					  <Link href="/logout">
					  <a className="nav-link" role="button">Logout</a>
					  </Link>
				 	</React.Fragment>
					  :
					  <Link href="/logout">
					  <a className="nav-link" role="button">Logout</a>
					  </Link>
					 }
				 </React.Fragment>
		  		:
				 <React.Fragment>
					  <Link href="/login">
					  <a className="nav-link" role="button">Login</a>
					  </Link>
					  <Link href="/register">
					  <a className="nav-link" role="button">Register</a>
					  </Link>
				  </React.Fragment>
				  }
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
		)
}
// export default class NaviBar extends Component {
// 	render(){
// 		return (
// 		<Navbar bg="light" expand="lg">
// 		  <Navbar.Brand href="#home">Zuitter</Navbar.Brand>
// 		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
// 		  <Navbar.Collapse id="basic-navbar-nav">
// 		    <Nav className="ml-auto">
// 		      <Nav.Link href="#home">Home</Nav.Link>
// 		      <Nav.Link href="#courses">Courses</Nav.Link>
// 		    </Nav>
// 		  </Navbar.Collapse>
// 		</Navbar>
// 			)
// 	}
// }