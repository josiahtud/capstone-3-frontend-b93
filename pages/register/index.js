import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function index() {

	//State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {
		// prevents o
		e.preventDefault();

		// Clear input fields
		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword1('');
		setPassword2('');

		console.log('Thank you for registering!');
	// }

	if((email !== "" && password1 !== "" && password1 !=="") && (password1 === password2)) {
			fetch('https://vast-ravine-42123.herokuapp.com/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === false) {

				fetch('https://vast-ravine-42123.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,

					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if (data === true) {

						alert("Registered Successfully");

						// Redirect to login
						window.location.replace("./login");

					} else {

						alert("Something went wrong.")

					}

				})


			} else {

				alert("Email address already exists. You may proceed to login.");

			}

		})

}

}

	useEffect(() => {

	//Validation to enable submit button when all fields are populated and both passwords match

	if((email !== "" && password1 !== "" && password1 !=="") && (password1 === password2)) {
		setIsActive(true);
	} else {
		setIsActive(false);
	}



	},[email, password1, password2])





	return (

		<Form onSubmit={(e) => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="firstName"
					placeholder="Enter First Name"
					value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="lastName"
					placeholder="Enter Last Name"
					value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
					required
				/>
			</Form.Group>


			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="mobileNo"
					placeholder="Enter Mobile Number"
					value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>


			<Form.Group controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
                    onChange={(e) => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>



			{isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
				Submit
				</Button>
			}
		</Form>
		)

}
