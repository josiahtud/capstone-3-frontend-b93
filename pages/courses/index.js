import React, { useContext } from 'react';
import Head from 'next/head';
import { Button, Table } from 'react-bootstrap';
import Course from '../../components/Course';
import coursesData from '../../data/courses';
import UserContext from '../../UserContext';

export default function index() {

	const { user } = useContext(UserContext);
	console.log(coursesData)

	// cards for normal users
	const courses = coursesData.map(course => {
		console.log(course.name);

	if(course.onOffer){
		return(
			<Course key={course.id} course={course}/>
			)
	} else {return null;}
	})
	//table rows for admin 
	const coursesRows = coursesData.map(course => {
		return(
			<tr key={course.id}>
				<td>{course.id}</td>
				<td>{course.name}</td>
				<td>Php {course.price}</td>
				<td>{course.onOffer ? 'Open' : 'Closed'}</td>
				<td>{course.start_date}</td>
				<td>{course.end_date}</td>
				<td>
					<Button variant="warning">Update</Button>
					<Button variant="danger" className="ml-3">Disable</Button>
				</td>
			</tr>



			)
	})

	return ( 
	user.isAdmin === true ?
		<React.Fragment>
			<Head>
				<title>Courses Admin Dashboard</title>
			</Head>
			<h1> Course Dashboard </h1>
			<Table striped bordered hover className="text-center">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				{coursesRows}
				</tbody>

			</Table>
		</React.Fragment>
		:
		<React.Fragment>
			<Head>
				<title>Courses Admin Dashboard</title>
			</Head>
			{courses}
		</React.Fragment>
	)
}

