import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Balance from '../components/Balance';
import IncomeExpenses from '../components/IncomeExpenses';
import NewRecord from '../components/NewRecord';
import ViewRecords from '../components/ViewRecords';

export default function Home(){

  const data = {
    title: "Budget Tracker Tool",
    content: "Keep track of your income and expenses for a sound budget",
    destination: "/courses",
    label: "Learn more!"
  }

  return (
  <React.Fragment>
    <Banner data={data} /> 

      <Balance />
      <IncomeExpenses />
      <NewRecord />
      <ViewRecords />   



    </React.Fragment>
  )
}

//      <Highlights />