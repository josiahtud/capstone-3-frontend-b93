import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NaviBar from '../components/NaviBar';
import { UserProvider } from '../UserContext';

import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {

	// State hook for the user details
	const [user, setUser] = useState({
		email: null,
		isAdmin: null
	})

		// Function to clear the local storage
	const unsetUser = () => {
		localStorage.clear();
	
		// Changes the value of the user back to it's original value
	setUser({ 
		email: null,
		isAdmin: null
	 });

	}
	console.log(user);
	useEffect(() => {
		setUser({
			email: localStorage.getItem('email'),
			// added a condition to convert the string data type into boolean
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})
	}, [])

	return (
		<React.Fragment>
			<UserProvider value={{user, setUser, unsetUser}}>
				<NaviBar />
				<Container className="my-5">
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</React.Fragment>
		)
}

export default MyApp
