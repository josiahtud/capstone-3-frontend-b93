		import React, { useState, useEffect } from 'react';
		import Form from 'react-bootstrap/Form';
		import Button from 'react-bootstrap/Button';
		import AppHelper from '../../app.helper';

// //"window.location.search" returns the query string
// console.log(window.location.search);

// //Instantiate a URLSearchParams object so we can exceute methods to access specific parts of the computer string
// let params = new URLSearchParams(window.location.search);

// // the "has" method checks if the "courseId" key exists in the URL query string
// // The method returns true if the key exists
// console.log(params.has('courseId'));

// // The "get" method returns the value of the key passed in an argument
// console.log(params.get('courseId'));

// let courseId = params.get('courseId');

export default function create() {

const [recordCategory, setRecordCategory] = useState('');
const [recordName, setRecordName] = useState('');
const [recordAmount, setRecordAmount] = useState(0);

const [isActive, setIsActive] = useState(false);

		function createRecord(e) {
		        e.preventDefault();

		        console.log(`${recordCategory} with ${recordName} for PhP ${recordAmount} per slot.`);
		        // console.log(`${category} with ID: ${recordId} and ${description} is set to start on ${createdOn} for PhP ${amount} per slot.`);
		        // console.log('token');
		        // setRecordId('');
		        setRecordCategory('');
		        setRecordName('');
		        setRecordAmount(0);
		        // setDate('');


	fetch('https://vast-ravine-42123.herokuapp.com/api/users/addRecord',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${localStorage.getItem('token')}`
		}, 
		body: JSON.stringify({
			// recordId : recordId,
			recordCategory : recordCategory,
			recordName : recordName,
			recordAmount : recordAmount

		})
	})
	.then(res => res.json())
	.then(data => {
	console.log(data);

	if(data === true) {
		alert("Thank you  for enrolling! See you in class!");
		window.location.replace("/");
	} else {
		alert("something went wrong");
	}

	})


} 

	useEffect(() => {

	//Validation to enable submit button when all fields are populated and both passwords match

	if(recordCategory !== "" && recordName !== "" && recordAmount !==0) {
		setIsActive(true);
	} else {
		setIsActive(false);
	}

	},[recordCategory, recordName, recordAmount])



		    return (
		        <Form onSubmit={(e) => createRecord(e)}>
		        <h4> Create New Record </h4>
		        <p></p>
{/*		            <Form.Group controlId="recordId">
		                <Form.Label>Record ID:</Form.Label>
		                <Form.Control
		                    type="text"
		                    placeholder="Enter record ID"
		                    value={recordId}
		                    onChange={e => setRecordId(e.target.value)}
		                    required
		                />
		            </Form.Group>*/}

		            <Form.Group controlId="recordCategory">
		                <Form.Label>Record Category:</Form.Label>
		                <Form.Control
		                    as="select"
		                    placeholder="Enter record category: Income or Expenses"
		                    value={recordCategory}
		                    onChange={e => setRecordCategory(e.target.value)}
		                    
		                    required
		                    >

		                <option>Income</option>
		                <option>Expenses</option>	
		                </Form.Control>


		            </Form.Group>

		            <Form.Group controlId="recordName">
		                <Form.Label>Record Name:</Form.Label>
		                <Form.Control 
		                    type="text"
		                    placeholder="Enter Record Name"
		                    value={recordName}
		                    onChange={e => setRecordName(e.target.value)}
		                    required
		                />

		            </Form.Group>



		            <Form.Group controlId="recordAmount">
		                <Form.Label>Amount:</Form.Label>
		                <Form.Control
		                    type="number"
		                    value={recordAmount}
		                    onChange={e => setRecordAmount(e.target.value)}
		                    required
		                />
		            </Form.Group>


			{isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
				Submit
				</Button>
			}


		        </Form>
		    )
		}




