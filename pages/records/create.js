		import React, { useState, useEffect } from 'react';
		import Form from 'react-bootstrap/Form';
		import Button from 'react-bootstrap/Button';
		import AppHelper from '../../app.helper';
		// import UserContext from '../../UserContext';
		// import { UserProvider } from '../../UserContext';
		
		export default function create() {


		    //declare form input states
		    // const [recordId, setRecordId] = useState('');

		    const [category, setCategory] = useState('');
		    const [name, setName] = useState('');
		    const [amount, setAmount] = useState(0);
		    // const [createdOn, setDate] = useState(0);

			// State to determine whether submit button is enabled or not
			const [isActive, setIsActive] = useState(false);

		    //function for processing creation of a new course
		    function createRecord(e) {
		        e.preventDefault();

		        console.log(`${name} with ${category} for PhP ${amount} per slot.`);
		        // console.log(`${category} with ID: ${recordId} and ${description} is set to start on ${createdOn} for PhP ${amount} per slot.`);
		        // console.log('token');
		        // setRecordId('');
		        setCategory('');
		        setName('');
		        setAmount(0);
		        // setDate('');


	fetch('https://vast-ravine-42123.herokuapp.com/api/records', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			category: category,
			name: name,
			amount: amount
		})
	})
	.then(res => res.json())
	.then(data => {


		if(data === true){

			alert("Record successfully added!");
			window.location.replace("/");

		} else {

			alert("Something went wrong.");

		}

	})


} 

	useEffect(() => {

	//Validation to enable submit button when all fields are populated and both passwords match

	if(category !== "" && name !== "" && amount !==0) {
		setIsActive(true);
	} else {
		setIsActive(false);
	}

	},[category, name, amount])



		    return (
		        <Form onSubmit={(e) => createRecord(e)}>
		        <h4> Create New Record </h4>
		        <p></p>
{/*		            <Form.Group controlId="recordId">
		                <Form.Label>Record ID:</Form.Label>
		                <Form.Control
		                    type="text"
		                    placeholder="Enter record ID"
		                    value={recordId}
		                    onChange={e => setRecordId(e.target.value)}
		                    required
		                />
		            </Form.Group>*/}

		            <Form.Group controlId="category">
		                <Form.Label>Record Category:</Form.Label>
		                <Form.Control
		                    as="select"
		                    placeholder="Enter record category: Income or Expenses"
		                    value={category}
		                    onChange={e => setCategory(e.target.value)}
		                    
		                    required
		                    >

		                <option>Income</option>
		                <option>Expenses</option>	
		                </Form.Control>


		            </Form.Group>

		            <Form.Group controlId="name">
		                <Form.Label>Record Name:</Form.Label>
		                <Form.Control 
		                    type="text"
		                    placeholder="Enter Record Name"
		                    value={name}
		                    onChange={e => setName(e.target.value)}
		                    required
		                />

		            </Form.Group>



		            <Form.Group controlId="amount">
		                <Form.Label>Amount:</Form.Label>
		                <Form.Control
		                    type="number"
		                    value={amount}
		                    onChange={e => setAmount(e.target.value)}
		                    required
		                />
		            </Form.Group>


			{isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
				Submit
				</Button>
			}


		        </Form>
		    )
		}


		// import React, { useState, useEffect } from 'react';
		// import Form from 'react-bootstrap/Form';
		// import Button from 'react-bootstrap/Button';

		// export default function create() {
		//     //declare form input states
		//     const [recordId, setRecordId] = useState('');
		//     const [category, setCategory] = useState('');
		//     const [description, setDescription] = useState('');
		//     const [amount, setAmount] = useState(0);
		//     const [createdOn, setDate] = useState(0);

		// 	// State to determine whether submit button is enabled or not
		// 	const [isActive, setIsActive] = useState(false);

		//     //function for processing creation of a new course
		//     function createRecord(e) {
		//         e.preventDefault();

		//         console.log(`${category} with ID: ${recordId} is set to start on ${createdOn} for PhP ${amount} per slot.`);

		//         setRecordId('');
		//         setCategory('');
		//         setDescription('');
		//         setAmount(0);
		//         setDate('');


		//     fetch('http://localhost:4000/api/records', {
		// 			method: 'POST',
		// 			headers: {
		// 				'Content-Type': 'application/json'
		// 			},
		// 			body: JSON.stringify({
		// 				category: category,
		// 				description: description,
		// 				amount: amount,


		// 			})
		// 		})
		// 		.then(res => res.json())
		// 		.then(data => {

		// 			console.log(data);

		// 	if (data === true) {

		// 				alert("Record Successfully Created");

		// 				// Redirect to login
		// 				window.location.replace("/");

		// 			} else {

		// 				alert("Something went wrong.")

		// 			}

		// 		})


		// 	} 