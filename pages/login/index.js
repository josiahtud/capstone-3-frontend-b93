import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import AppHelper from '../../app.helper';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import usersData from '../../data/users';

export default function index() {
    return(
        <View title={ 'Login ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Login</h3>
                    <LoginForm/>
                </Col>
            </Row>
        </View>
        )
}

const LoginForm = () => {
    console.log(usersData);
	const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);
    // State for page redirection

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();
        
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        }
        fetch(`${AppHelper.API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'does-not-exist'){
                    Swal.fire('Authentication Failed','User does not exist','error')
                } else if(data.error === 'incorrect-password'){
                    Swal.fire('Authentication Failed','User does not exist','error')
                } else if(data.error === 'login-type-error'){
                    Swal.fire('Login Type Error','You may have registered through a different login procedure, try the alternative login procedure','error')
                }
            }
        })

    }

        // const match = usersData.find(user => {
        //     return(user.email === email && user.password === password);
        // })

        // if(match) {
        //     console.log(match);

        //         // sets the email of the user in the local Storage
        //         localStorage.setItem('email', email);
        //         // will convert the boolean value into a string once passed into local storage
        //         localStorage.setItem('isAdmin', match.isAdmin);

        //         // sets the global user state to have properties from local storage
        //         setUser({
        //             email: localStorage.getItem('email'),
        //             isAdmin: match.isAdmin
        //         })

        //         // Clear input fields after submission
        //         setEmail('');
        //         setPassword('');

        //         Router.push('/courses');
 
        // } else {
        //     alert('Authentication failed. No match found.')}



        // // console.log(`${email} has been verified! Welcome back!`);




    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    const authenticateGoogleToken=(response)=>{
        console.log(response);

        const payload = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({ tokenId: response.tokenId})
        }
        fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {
            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error == 'google-auth-error') {
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error')
                } else if(data.error === 'login-type-error'){
                    Swal.fire('Login Type Error',
                        'You may have registered through a different login procedure','error')
                }
            }
        })
    };


    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: {Authorization: `Bearer ${ accessToken }`}
        }
        fetch(`${ AppHelper.API_URL}/users/details`,options)
        .then(AppHelper.toJSON)
        .then(data=>{
            setUser({ id: data._id, isAdmin: data.isAdmin})
            Router.push('/courses')
        })
    }

    return (
            <React.Fragment>
            <Head>
                <title>Authentication</title>
            </Head>
            <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <>
                <Button variant="danger" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
                    Submit
                </Button>

                <GoogleLogin
                //clientID=0AuthClient id from Cloud Google developer platform
                clientId="619304214942-jnkcj0947dfobbn5lsor86tonjrspkfk.apps.googleusercontent.com"
                buttonText="Login"
                //onSuccess runs a function which returns a Google user object which provides access to all the Google user method and details
                onSuccess={ authenticateGoogleToken }
                onFailure={ authenticateGoogleToken }

                cookiePolicy={ 'single_host_origin'}
                className="w-100 text-center d-flex justify-content-center"
                />

            </>
            }
        </Form>
    </React.Fragment>
    )
}

// global variable
// localStorage